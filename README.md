# RR-SDET-Assignment

This Cypress based sample tests project is implemented with the help of TypeScript and uses page-object model as the design pattern with BDD approach.

**Folder Structure:**

```

├── cypress
│   └── specs (feature files)
|   └── step-definitions (tests code)
│   └── ui-identifiers (page objects)
│   └── reports (load reports in HTML & JSON formats)
│   └── screenshots (capture screenshot on failure)
└── cypress.json (cypress global configuration)
└── testReport.html (mochawesome based HTML report)

```
   
**Salient Features:**

- Based on POM design pattern.
- Has the capability to run on various viewports such as desktop, tablet and mobile.
- Based on Cucumber / Gherkin standard.
- Cross-browser platform.
- Fully automated and provide both console and HTML report.
- Ability to take screenshot on failure.
- All tests are configured on CI/CD pipeline, hence doesn't require any manual intervention to run tests.

**Installation**:

- Clone the project
- Open terminal and run `npm install`

**Running tests**:

- Open terminal, run`npm run cy:open` to run tests in headed mode or run `npm run cy:run` to run in headless mode

Once the execution is complete, this is how the test report would look like,

<ins>Console</ins>:

<kbd><img src="/uploads/07cf00d6b50dd1f7a3b9b4a848a8e601/console.png" alt="Console Report" border="1" width=800></kbd>

<ins>HTML</ins>: 

<kbd><img src="/uploads/6b0b02a656f531f9bbfefc72ba610742/html.png" alt="HTML Report" border="1" width=800></kbd>

# (A) Testing strategy

The goal of test automation is to increase the effectiveness and efficiency of testing. Good automation makes testing faster, more systematic, and reduces human error. This includes: to reduce the number of test cases that testers have to perform manually as well as those that are challenging to perform manually, therefore saving time and effort.

The ideal test automation strategy is to follow the [Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html) mindset. The test pyramid is a way of thinking about how different kinds of automated tests should be used to create a balanced portfolio. The whole point is to offer immediate feedback to ensure that code changes do not disrupt existing features. This would essentially help both developers and QAs to create high-quality software. It reduces the time required for developers to identify if a change they introduced breaks the code.

This test automation pyramid mainly operates at three levels: Unit, Integration & UI.

<kbd><img src="/uploads/35ea89b86a8a8b1f3ec44aca86be0f7e/pyramid2.png" alt="Test Pyramid" border="1" width=500></kbd>


1. <ins>**Unit tests**</ins> form the base of the test pyramid. They should be frequent, and they should run fast.

2. <ins>**Integration tests**</ins> are the middle tier of the pyramid. These tests focus on interactions of your code with the outside world, such as databases and external services or microservices.
3.	<ins>**UI tests**</ins> top the test pyramid. They’re written from the perspective of a user and should test that your entire application is functioning from front-end to back-end.

<br></br>
**Why to use test pyramid?**

- Pyramid help QAs to define right priorities. If test scripts are written with a greater focus on the UI, then chances are that core business logic and back-end functionality is not sufficiently verified periodically. Hence, this affects product quality and leads to more work for the team.
- As TAT (turnaround time) of UI tests is high, it leads to lower test coverage overall. By adopting the pyramid, such situations can be completely avoided, and QAs can focus on writing quality tests keeping in mind all the 3 layers defined.
- Frequent collaboration between 3 Amigos (i.e. Developer, Tester & Product Owner)
- Since the pyramid is built to run the complex tests at the beginning, testers can manage time better, get better results and essentially make life easier for everyone involved. Therefore, it emphasize speed and efficacy.


## (I) Functional Testing:-

Test design techniques and methods used:

<ins>**Black-box testing:**</ins>

1. BVA (Boundary Value Analysis): To test the boundary of each coordinates while drawing map on canvas so that each point lies on exact co-ordinates, and if there is any deviation it will result into a failure.
2. Pairwise testing: This technique helped me to derive possible permutation and combination to test the authentication feature so that it increases the test coverage and reduces the number of executions of test cases. This would help to focus on those scenarios which will add a value to your test suite.
3. Equivalent Class Partitioning: Used this technique to draw an edge between two nodes so that the edge is partitioned properly and can be drawn accurately between two nodes. Partitioning the coordinates is the key to test the given feature appropriately. (Note: EP technique has more to offer, but I have used the very basic form to complete my test for this feature)
4. State Transition: This helped me to visualize the states of a system.
For example, what would be the state of user on providing valid credentials? And how the user can create a site and then transitioned it into a customized map of his/her choice? Such workflows I have noted and documented in the form of test cases in a feature file.
6. Decision Table: Used this technique to identify functionalities where the output depends on a combination of inputs. 
For example, Submit button (present on node element properties window) is enabled only when all inputs are entered by the user. This helped me to enlist all input test data keeping in mind all rules while providing different combination of inputs.

<ins>**White-box testing:**</ins> 
Though this technique is widely used during the product development, but I have used in designing test automation framework too to showcase their meaning and usage

1. Statement coverage: This involves the execution of all the statements at least once in the source code.

2. Branch coverage: Adopted this strategy to test all the branching statement in code. For example, viewports switch statement (refer step-definitions > common-steps file) has been verified using this method. This common function is being reused by every test cases in order to test features in all possible browser viewports (desktop, tablet & mobile).

<ins>**Experience-based testing:**</ins>

1. Error Guessing: As I get more comfortable in understanding the application, this method helped to predict where errors are more likely to appear based on the hands-on experience with the application so that I don’t miss such areas where defects are likely to appear while writing test cases.
For example, this technique helped me to find one of the defects where if I create a new map using .docx type image then it allows me to create it.

2. Exploratory testing: This approach is about learning, designing and executing tests simultaneously to uncover defects that are not easily covered in the scope of tests. This strategy helped me to find one blocker defect wherein the application doesn’t render on IE11 browser.


## (II) Non-Functional Testing:-

It involves testing the non-functional requirements of the application under test such as load, performance, stress, endurance, usability, scalability etc.

It involves testing the base technology (network, load balancer, application, database and web servers) periodically for the load levels you plan to support, and to better optimize your webserver and potentially avert business costs.

If the map contains too complex information then the time taken by a map to render on canvas can be noted in both view and edit mode. If the map fails to load based on the SLA, then this would give us the opportunity to thoroughly test the performance of application.

# (B) Miscellaneous

**(I) Feature file best practices:-**

1. Each scenario should be short & independent of each other. If the scenario is bit complex, split them into two.
2. The scenarios should be concise and to the point, so that the reader can quickly grasp the intent of the test.
3. Avoid “I” in step definitions. It is recommend writing steps in third person as this will remind you about the user's role in the application. 
4. Don’t use both first-person and third-person pronouns together in one scenario.
5. When you encounter a step which contains two actions, break them into two using 'And' keyword. There may be reasons for conjunctive steps. However, most of the time it’s better to avoid them.
6. If you are using the same steps at the beginning of all scenarios of a feature, put them into the feature’s Background as Background steps are run before each scenario. However, if you want to run a block of code before/after every scenario then cucumber hooks (such as `@Before` & `@After`) would be the ideal choice.
7. Make use of 'Scenario Outline' if there is a need to re-run the same scenario on multiple test data. But scenario outlines should focus on one behaviour at a time and use only the necessary variations.
8. Cucumber features/scenarios should be tagged properly so that they can be selected / grouped for automated test runs.

**(II) Defect List:-** The given [Issue list](https://docs.google.com/spreadsheets/d/1LYnUppwU4NDANFz5oaTnjPAMUKk3Lap_pXo7xK2Fu4A/edit?usp=sharing) contains all the defects found during testing. It also includes few suggestions which would help to build the product better.
