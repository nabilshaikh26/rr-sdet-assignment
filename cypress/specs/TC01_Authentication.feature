Feature: User Authentication

As a product manager, I want login and logout functionality to be implemented so that the user can access to their data securely.
    
    Scenario Outline: Login to application using invalid username
        Given User visits '/login' on '<viewport>' device
        When User enters username 'invalid' & password 'autobootstrap'
        And User clicks on Login button  
        Then User sees the error message as 'Error in logging in. Try again'
    
    Examples:

        | viewport |
        | desktop  |
        | mobile   |
    
    Scenario Outline: Login to application using invalid password
        Given User visits '/login' on '<viewport>' device
        When User enters username 'autobootstrap' & password 'invalid'
        And User clicks on Login button  
        Then User sees the error message as 'Error in logging in. Try again'
    
    Examples:

        | viewport |
        | desktop  |
        | mobile   |
    
    Scenario Outline: Login to application using invalid username & invalid password
        Given User visits '/login' on '<viewport>' device
        When User enters username 'invalid' & password 'invalid'
        And User clicks on Login button  
        Then User sees the error message as 'Error in logging in. Try again'
    
    Examples:

        | viewport |
        | desktop  |
        | mobile   |
    
    Scenario Outline: Login to application using valid credentials
        Given User visits '/login' on '<viewport>' device
        When User enters username 'autobootstrap' & password 'autobootstrap'
        And User clicks on Login button  
        Then User should land to home page successfully
        And User verifies the profile name to be appearing as 'autobootstrap'
    
    Examples:

        | viewport |
        | desktop  |
        | mobile   |
    
    Scenario Outline: Logout from the application
        Given User visits '/login' on '<viewport>' device
        And User authenticate himself using valid username 'autobootstrap' & valid password 'autobootstrap'
        And User should land to home page successfully
        When User hovers on profile and selects 'Logout' option
        Then User should land to 'login' page successfully
    
    Examples:

        | viewport |
        | desktop  |
        | mobile   |