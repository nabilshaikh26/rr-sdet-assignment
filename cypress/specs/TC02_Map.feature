Feature: Editing a map

As a user, I want the ability to perform add & delete operations on nodes, edges & region.
    
    Background: Login to application
        * User visits '/login' on 'desktop' device
        * User authenticate himself using valid username 'autobootstrap' & valid password 'autobootstrap'
        * User selects 'manage' option present on the left-hand side menu
        * User switch to 'Maps' tab
        * User selects 'tatsumi_5' site and chooses 'tatsumi' map
    
    Scenario: Creating and deleting a node
        Given User is on 'tatsumi_5' site and pointing to 'tatsumi' map in view mode
        And User edit the map
        When User draws a node on canvas at position x:900 & y:400
        Then User verifies a new node to be drawn
        When User deletes the newly created node
        Then User verifies the node should get deleted successfully
    
    Scenario: Creating and deleting an edge
        Given User is on 'tatsumi_5' site and pointing to 'tatsumi' map in view mode
        And User edit the map
        When User draws an edge between two nodes
        Then User verifies the edge should get created successfully
        When User deletes the edge
        Then User verifies the edge should get deleted successfully 
    
    Scenario: Creating and moving a region
        Given User is on 'tatsumi_5' site and pointing to 'tatsumi' map in view mode
        And User edit the map
        When User draws a new region on canvas
        Then User verifies a new region to be drawn on canvas
        When User moves the same region to different coordinate
        Then User verifies the region to be no longer in same place