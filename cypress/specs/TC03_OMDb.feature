Feature: Open Movie Database API

As a QA, I want to make a request to OMDb API so that I can search for a movie and verify the results being returned.
    
    Scenario Outline: Search movies by title
        Given url 'https://www.omdbapi.com/'
        When I query for a '<type>' by title '<name>' 
        Then the returning results should include the list of IDs followed by title
        And the results count should be 10

    Examples:

        | type  | name         |
        | movie | Harry Potter |
        | movie | harry potter |
        | movie | HaRrY pOtTeR |
        | Movie | Harry potter |