/* eslint-disable import/no-extraneous-dependencies */
/// <reference types="cypress" />

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';
import Login from '../ui-identifiers/Login';
import Menu from '../ui-identifiers/Menu';

const login = new Login();
const menu = new Menu();

Given('User enters username {string} & password {string}', (username, password) => {
  login
    .getUsername()
    .type(username);
  login
    .getPassword()
    .type(password);
});

When('User clicks on Login button', () => {
  login
    .getLoginButton()
    .click();
});

Then('User sees the error message as {string}', (errorMessage) => {
  cy
    .contains(errorMessage);
});

When('User hovers on profile and selects {string} option', (option) => {
  menu
    .getUserProfile()
    .trigger('mouseover')
    .then(() => {
      menu
        .getUserProfileOptions(option)
        .click({ force: true });
    });
});

Then('User should land to home page successfully', () => {
  cy
    .url()
    .should('equal', Cypress.config().baseUrl);
  menu
    .getRapyutaRobiticsLogo()
    .should('have.attr', 'alt', 'rapyuta robotics logo');
});

And('User verifies the profile name to be appearing as {string}', (profileName) => {
  menu
    .getUserProfile()
    .then((profile) => {
      expect(profile.text().trim()).to.equal(profileName);
    });
});

When('User should land to {string} page successfully', (pageName) => {
  cy
    .url()
    .should('equal', Cypress.config().baseUrl + pageName);
});
