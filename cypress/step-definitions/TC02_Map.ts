/* eslint-disable cypress/no-unnecessary-waiting */
/* eslint-disable no-shadow */
/* eslint-disable import/no-extraneous-dependencies */
/// <reference types="cypress" />

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';
import Manage from '../ui-identifiers/Manage';
import Maps from '../ui-identifiers/Maps';
import Menu from '../ui-identifiers/Menu';

const menu = new Menu();
const manage = new Manage();
const map = new Maps();

let nodeXCoordinateValue: any;
let nodeYCoordinateValue: any;
let regionXCoordinateValue: any;
let regionYCoordinateValue: any;

/**
  * Note:-
  * Used static wait, cy.wait(), wherever applicable even though it is not recommended.
  * Map will distort from it's position if we dont use it and it will result into flaky tests.
  */

And('User selects {string} option present on the left-hand side menu', (option) => {
  menu
    .getLeftHandSideMenu(option)
    .click();
});

And('User switch to {string} tab', (tab) => {
  manage
    .getManageOption(tab)
    .click();
});

And('User selects {string} site and chooses {string} map', (value, name) => {
  menu
    .getSiteFilter()
    .trigger('mouseover');
  menu
    .getSiteFilterOptions(value)
    .click();
  map
    .getMap(name)
    .click();
});

Given('User is on {string} site and pointing to {string} map in view mode', (siteString, mapString) => {
  menu
    .getSiteFilter()
    .then((filter) => {
      expect(filter).contain.text(siteString);
    });
  map
    .getCanvasMapName()
    .should((mapHeader: any) => {
      expect(mapHeader.text().trim()).to.equal(mapString);
    });
});

And('User edit the map', () => {
  map
    .getButtonByName('Edit map')
    .click();
});

When('User draws a node on canvas at position x:{int} & y:{int}', (xaxis, yaxis) => {
  nodeXCoordinateValue = xaxis;
  nodeYCoordinateValue = yaxis;
  map
    .getNodeMenuOption()
    .click();
  cy
    .wait(2000);
  map
    .getCanvas()
    .trigger('mousedown', nodeXCoordinateValue, nodeYCoordinateValue)
    .trigger('mouseup', nodeXCoordinateValue, nodeYCoordinateValue);
});

Then('User verifies a new node to be drawn', () => {
  // selecting a node

  map
    .getSelectMenuOption()
    .click();
  cy
    .wait(2000);
  map
    .getCanvas()
    .trigger('mousedown', nodeXCoordinateValue, nodeYCoordinateValue)
    .trigger('mouseup', nodeXCoordinateValue, nodeYCoordinateValue);

  // Verifying the coordinate axes

  map
    .getXAxisCoordinate()
    .then((xcoordinate) => {
      expect(xcoordinate.val()).to.equal('20.644023676316984');
    });
  map
    .getYAxisCoordinate()
    .then((ycoordinate) => {
      expect(ycoordinate.val()).to.equal('4.984311454638611');
    });
});

When('User deletes the newly created node', () => {
  map
    .getNodeMenuOption()
    .click();
  cy
    .wait(2000);
  map
    .getCanvas()
    .trigger('mousedown', nodeXCoordinateValue, nodeYCoordinateValue)
    .trigger('mouseup', nodeXCoordinateValue, nodeYCoordinateValue);

  map
    .getSelectMenuOption()
    .click();
  cy
    .wait(2000);
  map
    .getCanvas()
    .trigger('mousedown', nodeXCoordinateValue, nodeYCoordinateValue)
    .trigger('mouseup', nodeXCoordinateValue, nodeYCoordinateValue)
    .type('{del}')
    .type('{del}');
});

Then('User verifies the node should get deleted successfully', () => {
  map
    .getElementPropertyPlaceholderText()
    .should('have.text', 'Select an object to see format options');
});

When('User draws an edge between two nodes', () => {
  map
    .getNodeMenuOption().click();
  cy
    .wait(2000);

  // Creating first node
  map
    .getCanvas()
    .trigger('mousedown', 900, 550)
    .trigger('mouseup', 900, 550);

  // Creating second node
  map
    .getCanvas()
    .trigger('mousedown', 900, 600)
    .trigger('mouseup', 900, 600);

  // Drawing an edge between two nodes
  map
    .getEdgeMenuOption()
    .click();
  cy
    .wait(2000);
  map.getCanvas()
    .trigger('mousedown', 900, 550)
    .trigger('mousemove', 900, 600)
    .trigger('mousedown', 900, 600, { force: true })
    .trigger('mouseleave', 900, 600, { force: true });

  // Selecting newly created edge to verify
  cy
    .wait(2000);
  map
    .getSelectMenuOption().click();
  map
    .getCanvas()
    .trigger('mousedown', 900, 570)
    .trigger('mouseup', 900, 570);
});

Then('User verifies the edge should get created successfully', () => {
  map
    .getSelectedItemName()
    .should('have.text', 'Edge');
});

When('User deletes the edge', () => {
  cy
    .wait(2000);
  map
    .getCanvas()
    .trigger('click', 900, 570, { force: true })
    .type('{del}');
});

Then('User verifies the edge should get deleted successfully', () => {
  map
    .getElementPropertyPlaceholderText()
    .should('have.text', 'Select an object to see format options');
});

When('User draws a new region on canvas', () => {
  // Drawing a region
  map
    .getRegionMenuOption()
    .click();
  cy
    .wait(2000);
  map
    .getCanvas()
    .trigger('mousedown', { clientX: 1000 })
    .trigger('mousemove', { clientY: 800 })
    .trigger('mouseup', { clientY: 800 });

  // Selecting a region
  map
    .getCanvas()
    .trigger('mousedown', { clientX: 885, clientY: 750 })
    .trigger('mouseup', { clientX: 885, clientY: 750 });
});

Then('User verifies a new region to be drawn on canvas', () => {
  map
    .getSelectedItemName()
    .should('have.text', 'Regions');
  map
    .getXAxisCoordinate()
    .invoke('attr', 'value')
    .then((xcoordinate) => {
      regionXCoordinateValue = xcoordinate;
    });
  map
    .getYAxisCoordinate()
    .invoke('attr', 'value')
    .then((ycoordinate) => {
      regionYCoordinateValue = ycoordinate;
    });
});

When('User moves the same region to different coordinate', () => {
  cy
    .wait(2000);
  map
    .getCanvas()
    .trigger('mousedown', { clientX: 900 })
    .trigger('mousemove', { clientY: 700 })
    .trigger('mouseup', { clientY: 700 });
});

When('User verifies the region to be no longer in same place', () => {
  map
    .getSelectedItemName()
    .should('have.text', 'Regions');
  map
    .getXAxisCoordinate()
    .invoke('attr', 'value')
    .then((currentRegionXCoordinateValue) => {
      expect(regionXCoordinateValue).to.be.not.equal(currentRegionXCoordinateValue);
    });
  map
    .getYAxisCoordinate()
    .invoke('attr', 'value')
    .then((currentRegionYCoordinateValue) => {
      expect(regionYCoordinateValue).to.be.not.equal(currentRegionYCoordinateValue);
    });
});
