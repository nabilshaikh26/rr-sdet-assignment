/* eslint-disable import/no-extraneous-dependencies */
/// <reference types="cypress" />

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';
import { searchMovies } from '../support/api-requests';

let baseURL: string;

Given('url {string}', (url) => {
  baseURL = url;
});

When('I query for a {string} by title {string}', (movieType, movieTitle) => {
  searchMovies(
    baseURL, {
      s: movieTitle,
      type: movieType,
      apikey: Cypress.env('apiKeyValue'),
    },
  ).as('searchMoviesByTitle');
});

Then('the returning results should include the list of IDs followed by title', () => {
  cy
    .get('@searchMoviesByTitle')
    .then((response: any) => {
      response.forEach((item: any) => {
        cy.log(`ID - ${item.imdbID} => Title - ${item.Title}`);
      });
    });
});

And('the results count should be {int}', (count) => {
  cy
    .get('@searchMoviesByTitle')
    .then((items: any) => {
      expect(items.length).to.equal(count);
    });
});
