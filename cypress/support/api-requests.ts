/* eslint-disable import/prefer-default-export */
/// <reference types="cypress" />

import { identity, pickBy } from 'lodash';

export const searchMovies = (url: string, searchParams: Record<string, string>) => {
  const urlParams = pickBy(searchParams, identity);
  const urlSearchParams = new URLSearchParams(urlParams);
  return cy.request(`${url}${'?'}${urlSearchParams}`)
    .then((response: any) => response.body.Search);
};
