/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Login {
  getUsername() {
    return cy.get('[data-id=username]');
  }

  getPassword() {
    return cy.get('[data-id=password]');
  }

  getLoginButton() {
    return cy.get('.sc-bdnxRM');
  }
}

export default Login;
