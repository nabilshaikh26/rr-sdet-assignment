/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Manage {
  getManageOption(option: string) {
    return cy.get('.ant-tabs-nav-list').contains(option);
  }
}

export default Manage;
