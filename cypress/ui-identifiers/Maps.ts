/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Maps {
  getMap(name: string) {
    return cy.get('.rramr-list-card__details').children().contains(name);
  }

  getButtonByName(name: string) {
    return cy.get('.ant-btn').contains(name);
  }

  getCanvas() {
    return cy.get('.konvajs-content > :nth-child(4)', { timeout: 10000 });
  }

  getSelectMenuOption() {
    return cy.get('.sc-fTFMiz', { timeout: 10000 }).eq(0);
  }

  getNodeMenuOption() {
    return cy.get('.sc-fTFMiz', { timeout: 10000 }).eq(1);
  }

  getEdgeMenuOption() {
    return cy.get('.sc-fTFMiz', { timeout: 10000 }).eq(2);
  }

  getRegionMenuOption() {
    return cy.get('.sc-fTFMiz', { timeout: 10000 }).eq(4);
  }

  getXAxisCoordinate() {
    return cy.get('#root_pos_x');
  }

  getYAxisCoordinate() {
    return cy.get('#root_pos_y');
  }

  getElementPropertyPlaceholderText() {
    return cy.get('.sc-jVSGNQ');
  }

  getSelectedItemName() {
    return cy.get('.ant-collapse-item-active > .ant-collapse-header');
  }

  getCanvasMapName() {
    return cy.get('.sc-kGVuwA').find('h2');
  }
}

export default Maps;
