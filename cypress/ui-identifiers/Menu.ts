/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Menu {
  getUserProfile() {
    return cy.get('.sc-amiJK');
  }

  getUserProfileOptions(option: string) {
    return cy.get('.user-menu').contains(option);
  }

  getRapyutaRobiticsLogo() {
    return cy.get('.sc-dvXYtj');
  }

  getLeftHandSideMenu(menu: string) {
    return cy.get(`[href="/${menu}"]`);
  }

  getSiteFilter() {
    return cy.get('.button-header');
  }

  getSiteFilterOptions(filter: string) {
    return cy.get('.ant-dropdown-menu-root').children().contains(filter);
  }
}

export default Menu;
